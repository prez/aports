# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-beautifulsoup4
_pkgname=beautifulsoup4
pkgver=4.8.2
pkgrel=0
pkgdesc="A Python HTML/XML parser"
url="http://www.crummy.com/software/BeautifulSoup/index.html"
arch="noarch"
license="MIT"
depends="python3 py3-soupsieve"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/b/beautifulsoup4/beautifulsoup4-$pkgver.tar.gz
	fix-tests-with-syntax-error.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-beautifulsoup4" # Backwards compatibility
provides="py-beautifulsoup4=$pkgver-r$pkgrel" # Backwards compatibility

case "$CARCH" in
mips*)	options="!check" ;;
esac

build() {
	2to3 -w --no-diff bs4
	python3 setup.py build
}

check() {
	python3 -m unittest discover -s bs4
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="aab90eb22614e9a2fb47792389a951b3ad7faf9791e15984a6c27a38be6f3d0ccbf3a9753d9afb5c70105097ac1a9c5f9e07206e12c6d4cb7a7f6d4e90ec18c2  beautifulsoup4-4.8.2.tar.gz
a5274b512daf2a7297073553a054a49bb4d1a2f96137cdde108ce2235e74a8c0d0418bb6597088519972864336d1ed2a63a8dada73b53a50bdc7f72b762fe19d  fix-tests-with-syntax-error.patch"
